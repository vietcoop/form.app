<?php

namespace GO1\Bundle\FormBuilderBundle;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GoCatalyzeFormBuilderBundle extends Bundle
{

    public function boot()
    {
        Type::addType('entity_types', 'GO1\Bundle\FormBuilderBundle\Entity\Column\EntityTypes');

        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $em
            ->getConnection()
            ->getDatabasePlatform()
            ->registerDoctrineTypeMapping('entity_types', 'entity_types');
    }

}
