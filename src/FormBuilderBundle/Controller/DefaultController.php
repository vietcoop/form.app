<?php

namespace GO1\Bundle\FormBuilderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GoCatalyzeFormBuilderBundle:Default:index.html.twig', array('name' => $name));
    }
}
