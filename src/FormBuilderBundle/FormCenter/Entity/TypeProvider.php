<?php

namespace GO1\Bundle\FormBuilderBundle\FormCenter\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GO1\FormCenter\Entity\Provider\EntityTypeProviderBase;
use GO1\FormCenter\Entity\Type\EntityTypeInterface;

class TypeProvider extends EntityTypeProviderBase
{

    /** @var string */
    protected $name = 'go1_form_builder_db';

    /** @var string */
    protected $humanName = '[GO1.FormBuilder] Database';

    /** @var EntityManager */
    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * {@inheritdoc}
     * @return EntityTypeInterface[]
     */
    public function getEntityTypes()
    {
        $repos = $this->entityManager->getRepository('GO1\Bundle\FormBuilderBundle\Entity\EntityType');
        return $repos->findBy([], 'machineName');
    }

}
