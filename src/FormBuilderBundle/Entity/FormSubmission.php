<?php

namespace GO1\Bundle\FormBuilderBundle\Entity;

use GO1\FormCenter\Form\Submission\FormSubmissionBase;

class FormSubmission extends FormSubmissionBase
{

    /** @return int */
    private $id;

    /**
     * Get ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
