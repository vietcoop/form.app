<?php

namespace GO1\Bundle\FormBuilderBundle\Entity;

use GO1\FormCenter\Form\FormBase;

/**
 * Form entity. See ORM mapping in ./Resources/config/Form.orm.yml
 */
class Form extends FormBase
{

    /** @return int */
    private $id;

    /**
     * Get ID.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
