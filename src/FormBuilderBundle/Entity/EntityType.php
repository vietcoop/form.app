<?php

namespace GO1\Bundle\FormBuilderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use GO1\FormCenter\Entity\Storage\EntityStorageHandlerInterface;
use GO1\FormCenter\Entity\Type\EntityTypeBase;
use GO1\Bundle\FormBuilderBundle\FormCenter\Entity\StorageHandler;

/**
 * EntityType entity.
 */
class EntityType extends EntityTypeBase
{

    public function __construct()
    {
        $this->fields = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return EntityStorageHandlerInterface
     */
    protected function getDefaultStorageHandler()
    {
        return new StorageHandler();
    }

}
