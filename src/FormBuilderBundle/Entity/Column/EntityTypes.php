<?php

namespace GO1\Bundle\FormBuilderBundle\Entity\Column;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonArrayType;
use GO1\FormCenter\Entity\Type\EntityTypeInterface;
use Symfony\Component\HttpKernel\Kernel;

class EntityTypes extends JsonArrayType
{

    public function getName()
    {
        return 'entity_types';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $_items = [];
        foreach ($value as $entity_type) {
            /* @var $entity_type EntityTypeInterface */
            $_items[] = $entity_type->getName();
        }

        return parent::convertToPHPValue($_items, $platform);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /* @var $kernel Kernel */
        global $kernel;

        $entity_types = [];
        foreach (parent::convertToDatabaseValue($value, $platform) as $entity_type_name) {
            $entity_types[] = $kernel->getContainer()->get('form_builder.manager');
        }

        return $entity_types;
    }

}
